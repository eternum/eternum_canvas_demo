//= require three.js/three.js
//=require  three.js/js/Detector.js
//=require three.js/js/StereoEffect.js
//=require three.js/js/DeviceOrientationControls.js
//=require three.js/js/DetectScreenSize.js
//=require three.js/js/libs/stats.min.js

$(document).ready( function() {
    if (!Detector.webgl) Detector.addGetWebGLMessage();

    var container;
    var info, stats, renderer, scene, camera, controls;
    var effect;
    var controls = undefined;
    var hasOrientation = function(evt) {
        if (!evt.alpha) {
            return;
        }
        window.removeEventListener('deviceorientation', hasOrientation, false);
        controls = new THREE.DeviceOrientationControls( camera );
        controls.connect();
    };

    var mesh, lightMesh, geometry;
    var spheres = [];

    var directionalLight, pointLight;

    var mouseX = 0, mouseY = 0;

    var windowHalfX = window.innerWidth / 2;
    var windowHalfY = window.innerHeight / 2;

    document.addEventListener('mousemove', onDocumentMouseMove, false);

    init();
    animate();

    function displayStats(){
        stats = new Stats();
        stats.domElement.style.position = 'absolute';
        stats.domElement.style.top = '0px';
        container.appendChild( stats.domElement );

    }
    function addContainer(){
        container = document.createElement('div');
        document.body.appendChild(container);
    }
    function addInfo(){
        document.body.style.cssText = 'font: 600 12pt monospace; margin: 0; overflow: hidden' ;
        var info = document.body.appendChild( document.createElement( 'div' ) );
        info.style.cssText = 'left: 10px; position: absolute; ';
        info.innerHTML = '<h3>' + document.title + '</h3>' +
        '<div id=msg>readout</div>';
    }

    function addRenderer(){
        renderer = new THREE.WebGLRenderer( { alpha: 1, antialias: true, clearColor: 0xffffff }  );
        renderer.setSize( window.innerWidth, window.innerHeight );
        container.appendChild( renderer.domElement );
    }
    function addScene(){
        scene = new THREE.Scene();
        scene.autoUpdate = false;
        window.scene=scene;
    }
    function loadMesh(file){
        var loader = new THREE.JSONLoader(); // init the loader util
        loader.load('/assets/'+file, function (geometry,mat) {
            var material = new THREE.MeshLambertMaterial({
                //map: THREE.ImageUtils.loadTexture('path_to_texture'),  // specify and load the texture
                colorAmbient: [0.480000026226044, 0.480000026226044, 0.480000026226044],
                colorDiffuse: [0.480000026226044, 0.480000026226044, 0.480000026226044],
                colorSpecular: [0.8999999761581421, 0.8999999761581421, 0.8999999761581421]
            });
            var materials = new THREE.MeshFaceMaterial( mat );
            var mesh = new THREE.Mesh(
                geometry,
                materials
            );
            mesh.receiveShadow = true;
            mesh.castShadow = true;
            mesh.rotation.y = -Math.PI/5;
            scene.add(mesh);
        });
    }
    function init() {
        var geometry, material, mesh;
        var container = document.body.appendChild( document.createElement( 'div' ) );
        addContainer();
        displayStats();
        addInfo();
        addRenderer();
        addScene();

        var geometry = new THREE.SphereGeometry(100, 32, 16);

        var path = "/assets/textures/cube/skybox/";
        var format = '.jpg';
        var urls = [
            path + 'px' + format, path + 'nx' + format,
            path + 'py' + format, path + 'ny' + format,
            path + 'pz' + format, path + 'nz' + format
        ];
        //scene.add(geometry);

        var textureCube = THREE.ImageUtils.loadTextureCube(urls, new THREE.CubeRefractionMapping());
        var material = new THREE.MeshBasicMaterial({color: 0xffffff, envMap: textureCube, refractionRatio: 0.95});
        camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 5000 );
        camera.position.set( 3,3,3 );
        window.camera=camera;
        scene.add( camera );
        effect = new THREE.StereoEffect( renderer );
        effect.separation = 0.2;
        effect.targetDistance = 50;
        effect.setSize( window.innerWidth, window.innerHeight );

        loadMesh("demo.json");
        var directionalLight = new THREE.DirectionalLight( 0xffffff, 0.5 );
        directionalLight.position.set( 3, 3, 0 );
        scene.add( directionalLight );

        var shader = THREE.ShaderLib["cube"];
        shader.uniforms["tCube"].value = textureCube;

        var material = new THREE.ShaderMaterial({

                fragmentShader: shader.fragmentShader,
                vertexShader: shader.vertexShader,
                uniforms: shader.uniforms,
                side: THREE.BackSide

            });
        mesh = new THREE.Mesh(new THREE.BoxGeometry(100000, 100000, 100000), material);
        scene.add(mesh);



        renderer = new THREE.WebGLRenderer();
        container.appendChild(renderer.domElement);


        window.addEventListener('deviceorientation', hasOrientation, false);
        window.addEventListener( 'touchend', function () {
            if ( isFullscreen === false ) {
                document.body.webkitRequestFullscreen();
                isFullscreen = true;
            } else {
                document.webkitExitFullscreen();
                isFullscreen = false;
            }
        } );
        window.addEventListener( 'resize', function () {
            camera.aspect = window.innerWidth / window.innerHeight;
            camera.updateProjectionMatrix();
            effect.setSize( window.innerWidth, window.innerHeight );
        }, false );
    }

    function onWindowResize() {

        windowHalfX = window.innerWidth / 2,
            windowHalfY = window.innerHeight / 2,

            camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        effect.setSize(window.innerWidth, window.innerHeight);

    }

    function onDocumentMouseMove(event) {

        mouseX = ( event.clientX - windowHalfX ) * 10;
        mouseY = ( event.clientY - windowHalfY ) * 10;

    }
    function animate() {
        requestAnimationFrame( animate );
        if ( controls !== undefined ) {
            controls.update();
            msg.innerHTML =
                '<table><tr><td>' +
                'oQ.w ' + controls.orientationQuaternion.w.toFixed(3) + '<br>' +
                'oQ.x ' + controls.orientationQuaternion.x.toFixed(3) + '<br>' +
                'oQ.y ' + controls.orientationQuaternion.y.toFixed(3) + '<br>' +
                'oQ.z ' + controls.orientationQuaternion.z.toFixed(3) + '<br>' +
                'aQ.w ' + controls.alignQuaternion.w.toFixed(3) + '<br>' +
                'aQ.x ' + controls.alignQuaternion.x.toFixed(3) + '<br>' +
                'aQ.y ' + controls.alignQuaternion.y.toFixed(3) + '<br>' +
                'aQ.z ' + controls.alignQuaternion.z.toFixed(3) + '<br>' +
                '</td>' +
                '<td>' +
                'alpha ' + controls.alpha.toFixed(3) + '<br>' +
                'beta ' + controls.beta.toFixed(3) + '<br>' +
                'gamma ' + controls.gamma.toFixed(3) + '<br>' +
                'movSpd ' + controls.movementSpeed + '<br>' +
                'rllSpd ' + controls.rollSpeed + '<br>' +
                '</td></tr></table>' +
                '';
        }
        effect.render( scene, camera );
    };
});